package com.vivo.domain;

import lombok.Getter;
import lombok.Setter;

import java.sql.Date;
@Setter@Getter
public class vivo_enshrine {
	private	int v_eid;
	private	int v_gid;
	private	Date v_time;
	private	String v_uid;

	@Override
	public String toString() {
		return "vivo_enshrine{" +
				"v_eid=" + v_eid +
				", v_gid=" + v_gid +
				", v_time=" + v_time +
				", v_uid='" + v_uid + '\'' +
				'}';
	}
}
