package com.vivo.domain;

import lombok.Getter;
import lombok.Setter;

@Setter@Getter
public class vivo_divisionGoods {
    private Integer v_did;
    private String v_image;
    private Integer v_sid;

    @Override
    public String toString() {
        return "vivo_divisionGoods{" +
                "v_did=" + v_did +
                ", v_image='" + v_image + '\'' +
                ", v_sid=" + v_sid +
                '}';
    }
}
