package com.vivo.domain;

import lombok.Getter;
import lombok.Setter;

@Setter@Getter
public class vivo_orderdetails {
    private Integer v_orderNo;
    private String v_myOrderNo;
    private Integer v_goodId;
    private Integer v_count;
    private double v_price;
    private Integer v_editionNo;
    private Integer v_colorNo;


    @Override
    public String toString() {
        return "vivo_orderdetails{" +
                "v_orderNo=" + v_orderNo +
                ", v_myOrderNo=" + v_myOrderNo +
                ", v_goodId=" + v_goodId +
                ", v_count=" + v_count +
                ", v_price=" + v_price +
                ", v_editionNo=" + v_editionNo +
                ", v_colorNo=" + v_colorNo +
                '}';
    }
}
