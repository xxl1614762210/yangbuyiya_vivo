package com.vivo.domain;

import lombok.Getter;
import lombok.Setter;

@Setter@Getter
public class vivo_GoodsState {
    private Integer v_gsid;
    private String v_gsName;

    @Override
    public String toString() {
        return "vivo_GoodsState{" +
                "v_gsid=" + v_gsid +
                ", v_gsName='" + v_gsName + '\'' +
                '}';
    }
}
