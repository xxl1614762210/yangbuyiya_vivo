package com.vivo.domain;

import lombok.Getter;
import lombok.Setter;

@Setter@Getter
public class vivo_myorder_vivo_orderdetails {

    private String v_myOrderNo;
    private String v_time;
    private Integer v_status;
    private String v_uid;
    private Double v_totlePrice;
    private Integer v_takeNo;
    private Integer v_goodid;
    private Integer v_count;
    private Double v_price;
    private String v_image;
    private String v_user;
    private String v_edName;
    private String v_colorName;

    @Override
    public String toString() {
        return "vivo_myorder_vivo_orderdetails{" +
                "v_myOrderNo='" + v_myOrderNo + '\'' +
                ", v_time='" + v_time + '\'' +
                ", v_status=" + v_status +
                ", v_uid='" + v_uid + '\'' +
                ", v_totlePrice=" + v_totlePrice +
                ", v_takeNo=" + v_takeNo +
                ", v_goodid=" + v_goodid +
                ", v_count=" + v_count +
                ", v_price=" + v_price +
                ", v_image='" + v_image + '\'' +
                ", v_user='" + v_user + '\'' +
                ", v_edName='" + v_edName + '\'' +
                ", v_colorName='" + v_colorName + '\'' +
                '}';
    }
}
