package com.vivo.domain;

import lombok.Getter;
import lombok.Setter;

@Setter@Getter
public class vivo_good_color {
	private	int v_cid;
	private	String v_name;
	private	int v_colorNo;

	@Override
	public String toString() {
		return "vivo_good_color{" +
				"v_cid=" + v_cid +
				", v_name='" + v_name + '\'' +
				", v_colorNo=" + v_colorNo +
				'}';
	}
}
