package com.vivo.domain;

import lombok.Getter;
import lombok.Setter;

@Setter@Getter
public class vivo_myorder_vivo_shoppingcart {

    private String v_myOrderNo;
    private String v_time;
    private double v_totlePrice;
    private Integer v_status; // 状态
    private Integer v_takeNo;// 收货地址id

    private	Integer v_tid;
    private	String v_consignee;
    private	String v_phone;
    private	String v_address;
    private	String v_receivingArea;

    private	String v_uid;
    private Integer v_cid;
    private String v_image;
    private String v_user;
    private String v_gparticulars;
    private Integer v_price;
    private Integer v_cprice;
    private String v_setMeal;
    private Integer v_quanity;
    private String v_colorName;
    private String v_edName;
    /* 添加字段需要 */
    private Integer v_gid;
    private Integer v_editionNo;
    private Integer v_colorNo;

    @Override
    public String toString() {
        return "vivo_myorder_vivo_shoppingcart{" +
                "v_myOrderNo=" + v_myOrderNo +
                ", v_time='" + v_time + '\'' +
                ", v_totlePrice=" + v_totlePrice +
                ", v_status=" + v_status +
                ", v_takeNo=" + v_takeNo +
                ", v_tid=" + v_tid +
                ", v_consignee='" + v_consignee + '\'' +
                ", v_phone='" + v_phone + '\'' +
                ", v_address='" + v_address + '\'' +
                ", v_receivingArea='" + v_receivingArea + '\'' +
                ", v_uid='" + v_uid + '\'' +
                ", v_cid=" + v_cid +
                ", v_image='" + v_image + '\'' +
                ", v_user='" + v_user + '\'' +
                ", v_gparticulars='" + v_gparticulars + '\'' +
                ", v_price=" + v_price +
                ", v_cprice=" + v_cprice +
                ", v_setMeal='" + v_setMeal + '\'' +
                ", v_quanity=" + v_quanity +
                ", v_colorName='" + v_colorName + '\'' +
                ", v_edName='" + v_edName + '\'' +
                ", v_gid=" + v_gid +
                ", v_editionNo=" + v_editionNo +
                ", v_colorNo=" + v_colorNo +
                '}';
    }
}
