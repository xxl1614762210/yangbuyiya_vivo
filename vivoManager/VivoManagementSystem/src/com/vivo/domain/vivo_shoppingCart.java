package com.vivo.domain;

import lombok.Getter;
import lombok.Setter;

@Getter@Setter
public class vivo_shoppingCart {
    private Integer v_cid;
    private String v_uid;
    private String v_image;
    private String v_user;
    private String v_gparticulars;
    private Integer v_price;
    private Integer v_cprice;
    private String v_setMeal;
    private Integer v_quanity;
    private String v_colorName;
    private String v_edName;
    /* �����ֶ���Ҫ */
    private Integer v_gid;
    private Integer v_editionNo;
    private Integer v_colorNo;

    @Override
    public String toString() {
        return "vivo_shoppingCart{" +
                "v_cid=" + v_cid +
                ", v_uid='" + v_uid + '\'' +
                ", v_image='" + v_image + '\'' +
                ", v_user='" + v_user + '\'' +
                ", v_gparticulars='" + v_gparticulars + '\'' +
                ", v_price=" + v_cprice +
                ", v_cprice=" + v_cprice +
                ", v_setMeal='" + v_setMeal + '\'' +
                ", v_quanity=" + v_quanity +
                ", v_colorName='" + v_colorName + '\'' +
                ", v_edName='" + v_edName + '\'' +
                ", v_gid=" + v_gid +
                ", v_editionNo=" + v_editionNo +
                ", v_colorNo=" + v_colorNo +
                '}';
    }
}
