package com.vivo.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class PageBean {

    // 当前是哪一页
    private Integer currentPage;
    // 共多少页
    private Integer totalPage;
    // 多少条记录
    private Integer totalCount;
    // 当前页商品
    private List<vivo_goods> goodsList = new ArrayList<vivo_goods>();

    private List<vivo_myorder_vivoOrder> vivo_myorder_vivoOrder = new ArrayList<vivo_myorder_vivoOrder>();

}
