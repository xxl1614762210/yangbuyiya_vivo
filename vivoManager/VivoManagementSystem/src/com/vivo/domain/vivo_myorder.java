package com.vivo.domain;

import lombok.Getter;
import lombok.Setter;

import java.math.BigInteger;
import java.util.Date;
@Getter@Setter
public class vivo_myorder {
    private String v_myOrderNo;
    private String v_time;
    private Integer v_takeNo;
    private int v_status;
    private String v_uid;
    private double v_totlePrice;

    @Override
    public String toString() {
        return "vivo_myorder{" +
                "v_myOrderNo=" + v_myOrderNo +
                ", v_time=" + v_time +
                ", v_takeNo=" + v_takeNo +
                ", v_status=" + v_status +
                ", v_uid='" + v_uid + '\'' +
                ", v_totlePrice=" + v_totlePrice +
                '}';
    }
}
