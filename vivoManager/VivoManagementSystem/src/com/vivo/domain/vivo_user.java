package com.vivo.domain;

import lombok.Getter;
import lombok.Setter;

import java.math.BigInteger;

@Getter@Setter
public class vivo_user {

    private String v_uid;
    private String v_user;
    private String v_phone;
    private String v_password;
    private String v_Email;
    private String v_headPortrait;
    private String v_sex;
    private String v_birthday;
    private String v_address;

    @Override
    public String toString() {
        return "vivo_user{" +
                "v_uid='" + v_uid + '\'' +
                ", v_user='" + v_user + '\'' +
                ", v_phone=" + v_phone +
                ", v_password='" + v_password + '\'' +
                ", v_Email='" + v_Email + '\'' +
                ", v_headPortrait='" + v_headPortrait + '\'' +
                ", v_sex='" + v_sex + '\'' +
                ", v_birthday='" + v_birthday + '\'' +
                ", v_address='" + v_address + '\'' +
                '}';
    }
}
