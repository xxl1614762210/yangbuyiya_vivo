package com.vivo.domain;

import lombok.Getter;
import lombok.Setter;

@Setter@Getter
public class vivo_d_o {
    private Integer v_did;
    private String v_image;
    private Integer v_oid;
    private String v_oName;

    @Override
    public String toString() {
        return "vivo_d_o{" +
                "v_did=" + v_did +
                ", v_image='" + v_image + '\'' +
                ", v_oid=" + v_oid +
                ", v_oName='" + v_oName + '\'' +
                '}';
    }
}
