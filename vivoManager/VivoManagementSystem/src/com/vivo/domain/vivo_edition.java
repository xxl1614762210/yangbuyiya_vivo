package com.vivo.domain;

import lombok.Getter;
import lombok.Setter;

@Getter@Setter
public class vivo_edition {
	private	int	v_eid;
	private	String v_name;
	private	String v_editionNo;

	@Override
	public String toString() {
		return "vivo_edition{" +
				"v_eid=" + v_eid +
				", v_name='" + v_name + '\'' +
				", v_editionNo='" + v_editionNo + '\'' +
				'}';
	}
}
