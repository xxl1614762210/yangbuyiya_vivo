package com.vivo.domain;

import lombok.Getter;
import lombok.Setter;

@Setter@Getter
public class vivo_myorder_vivoOrder {

    private String v_myOrderNo;
    private String v_time;
    private Integer v_status;
    private String v_consignee;
    private String v_phone;
    private String v_address;
    private String v_receivingArea;
    private Integer v_totlePrice;

    @Override
    public String toString() {
        return "vivo_myorder_vivoOrder{" +
                "v_myOrderNo='" + v_myOrderNo + '\'' +
                ", v_time='" + v_time + '\'' +
                ", v_status=" + v_status +
                ", v_consignee='" + v_consignee + '\'' +
                ", v_phone='" + v_phone + '\'' +
                ", v_address='" + v_address + '\'' +
                ", v_receivingArea='" + v_receivingArea + '\'' +
                ", v_totlePrice=" + v_totlePrice +
                '}';
    }
}
