package com.vivo.domain;

import lombok.Getter;
import lombok.Setter;

@Setter@Getter
public class vivo_admin {
	private	int v_aid;
	private	String	v_aname;
	private	String	v_apassword;

	@Override
	public String toString() {
		return "vivo_admin{" +
				"v_aid=" + v_aid +
				", v_aname='" + v_aname + '\'' +
				", v_apassword='" + v_apassword + '\'' +
				'}';
	}
}
