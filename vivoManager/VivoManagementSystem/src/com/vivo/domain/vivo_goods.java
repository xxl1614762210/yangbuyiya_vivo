package com.vivo.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class vivo_goods {
    private Integer v_gid;
    private String v_image;
    private String v_user;
    private String v_gparticulars;
    private String desc;
    private Integer v_host;
    private double v_price;
    private Integer v_inventory;
    private Integer v_sid;
    private Integer v_editionNo;
    private Integer v_colorNo;
    private Integer v_status;
    private String v_name; /* ϵ������ */


    @Override
    public String toString() {
        return "vivo_goods{" +
                "v_gid=" + v_gid +
                ", v_image='" + v_image + '\'' +
                ", v_user='" + v_user + '\'' +
                ", v_gparticulars='" + v_gparticulars + '\'' +
                ", desc='" + desc + '\'' +
                ", v_host=" + v_host +
                ", v_price=" + v_price +
                ", v_inventory=" + v_inventory +
                ", v_sid=" + v_sid +
                ", v_editionNo=" + v_editionNo +
                ", v_colorNo=" + v_colorNo +
                ", v_status=" + v_status +
                ", v_name='" + v_name + '\'' +
                '}';
    }
}
