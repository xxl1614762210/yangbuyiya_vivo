package com.vivo.domain;

import lombok.Getter;
import lombok.Setter;

@Getter@Setter
public class vivo_good_series {
    private Integer v_sid;
    private String v_name;

    @Override
    public String toString() {
        return "vivo_good_series{" +
                "v_sid=" + v_sid +
                ", v_name='" + v_name + '\'' +
                '}';
    }
}
